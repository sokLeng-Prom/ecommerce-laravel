<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //get
    function index(){
        //return Product::all();
        $products = Product::all();
        //dd($products); 
        return view('login')->with('products', $products);
    }

    function product(){
        //return Product::all();
        $products = Product::all();
        //dd($products); 
        return view('welcome')->with('products', $products);
    }

    function detail($id){
        // dd($id);
        $product = Product::find($id);
        return view('product_detail')->with('product', $product);
    }

    

    //post
    function store(Request $request) {
        //validation
        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'price' => 'required',
        ]);
       
        //request all the data
        return Product::create($request->all());
    }

    //search individual
    function show($id){
        return Product::find($id);
    }

    //put
    function update(Request $request,$id) {
        //find->update->give back
        $product = Product::find($id);
        $product->update($request->all());
        return $product;
    }

    //delete
    function destroy($id){
        Product::destroy($id);
    }

    function search($name){
        return Product::where('name', 'like', '%'.$name.'%')->get();
    }
}
